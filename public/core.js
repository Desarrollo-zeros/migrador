
var val = false;
var Data = "";
var valItems = false;
/*login php*/

$(document).ready(function () {
   
	if(localStorage.validarMigration){
        $("#iniciarSession").css("display","none");
        $("#panelM").css("display","block");
    }
    cargarDatosMigracion();
    buscarMigraciones();
});
$("#enLineaFrom").on("submit",function (form) {
    form.preventDefault();
    if($("#aceptarTerminos").val() == "enviar"){
        if(confirm("¿Esta Seguro de estar online  ?")){
            $("#btNCerrar").prop('disabled', true);
            $("#btnL").prop('disabled', true);
            enviarObjetos($("#character").val());
        }
    }
    else{
        $.notify("Debes escribir la palabra enviar para poder seguir");
    }
});
$("#mostarPersonaje").on("submit",function (form) {
    form.preventDefault();
    loaderVerMigracion(100);
	var id = $("#character").val();
    $('#view-modal').modal('show');
    $.ajax({
        url : localStorage.url + "migrador/buscarPersonajes",
        dataType : 'json',
        type: 'POST',
        data : {
            id: id
        },
        success : function (data) {

            $("#nombrePj").val(data.name);
            $("#nombreP").html(data.name);
            $("#nivelPj").html(data.level);
            if(data.class == 1)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_warrior.png');
            if(data.class == 2)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_paladin.png');
            if(data.class == 3)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_hunter.png');
            if(data.class == 4)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_rogue.png');
            if(data.class == 5)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_priest.png');
            if(data.class == 6)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_deathknight.png');
            if(data.class == 7)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_shaman.png');
            if(data.class == 8)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_mage.png');
            if(data.class == 9)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_warlock.png');
            if(data.class == 11)
                $("#clasesPj").attr("src",'public/clases/Ui-charactercreate-classes_druid.png');
            var gender =  (data.gender == 0) ? "male" : "female";
            if(data.race == 1)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_human-"+gender+".png");
            if(data.race == 2)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_orc-"+gender+".png");
            if(data.race == 3)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_dwarf-"+gender+".png");
            if(data.race == 4)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_nightelf-"+gender+".png");
            if(data.race == 5)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_undead-"+gender+".png");
            if(data.race == 6)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_tauren-"+gender+".png");
            if(data.race == 7)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_gnome-"+gender+".png");
            if(data.race == 8)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_troll-"+gender+".png");
            if(data.race == 10)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_bloodelf-"+gender+".png");
            if(data.race == 11)
                $("#racesPj").attr("src","public/race/Ui-charactercreate-races_draenei-"+gender+".png");

			
			 if($("#isMaster").val()){	
                $("#RecibirItems").html('<button onclick="return completarMigracion()" class="btn btn-sm btn-success col-md-12">Completar Migracion</a>');
                $("#EliminarMigracion").html('<button  onclick="return EliminarMigracion()" class="btn btn-sm btn-danger col-md-12">Eliminar Migracion</a>');
            }
            else{
                $("#RecibirItems").html('<button id="btnRecebir" onclick="return activarEnvioObjetos();" class="btn btn-sm btn-success col-md-12">Enviar Objetos</a>');
            }
			

            for(var i in Data){
                if(Data[i].guid == data.guid){
                    if(Data[i].estado == 1){
						$("#estadoPj").html("<p style='color: #00CC00;'>Migracion Exitosa</p>");
					}
                    else if(Data[i].estado == 2){
						
						if(!localStorage.migracionDirecta){
							$("#estadoPj").html("<p style='color: #0000FF;'>En espera: El encargado debe aceptar tu migracion</p>");
							$("#btnRecebir").prop('disabled', true);
						}
						else{
							 $("#estadoPj").html("<p style='color: #00CC00;'>Ya puede recibir tus Objetos, Debes </p>");
						}
						
					}
                    else if(Data[i].estado == 3){ 
						$("#estadoPj").html("<p style='color: red'>Error Comunicate con un Gm</p>");
					}
                    else if(Data[i].estado == 5){
                        $("#estadoPj").html("<p style='color: #00CC00;'>Ya puede recibir tus Objetos, Debes </p>");
						if(!localStorage.migracionDirecta){
							$("#btnRecebir").prop('disabled', false);
						}
                    }
                    else{
						$("#estadoPj").html("<p style='color: red;'>Error </p>");
					}
                }
            }
		
           

        }
    });



});
$("#verp").click(function () {
	$("#verVideo").css("display","none");
    $("#migrarPersonaje").css("display","none");
    $("#verPersonajeMigrados").css("display","block");
    buscarMigraciones();
    $("#preloader").css("display","block");
    loader(10,1);
});
$("#migrarp").click(function () {
	$("#verVideo").css("display","none");
    $("#verPersonajeMigrados").css("display","none");
    $("#migrarPersonaje").css("display","block");
    cargarDatosMigracion();
    $("#preloader").css("display","block");
    loader(10,1);
});
$("#enviarFile").on("submit",function (form) {
    form.preventDefault();
    if(confirm("Desea Comenzar Con la Migracion")){
        var formData = new FormData($(this)[0]);
        var img = document.getElementById('fileMigrador').files[0].name;

		if($("#guid").val() != "Seleccione el Personaje a migrar"){
			if(img === "zwgamers.lua"){
				$("#myModal").modal({backdrop: 'static', keyboard: false});
				LoaderFile(10000);
				$.ajax({
					url: localStorage.url + "migrador/recibirArchivoMigracion",
					type: 'POST',
					data: formData,
					async: true,
					cache: false,
					contentType: false,
					processData: false,
					dataType: 'json',
					success: function (data) {
						val = data;
						if(val == true || val == 1)
							$.notify("Personaje Migrado con exito, debe esperar mientras configuramos a tu cuenta","success");
						else if(val == -1)
							$.notify("Servidor offline","error");
						else if(val == -2)
							$.notify("Tu Version de WoW incorrecta, debes migrar de la version 3.3.5a","error");
						else if(val == -3)
							$.notify("No posees logros suficiente para poder migrar","error");
						else if(val == -4)
							$.notify("Tu jugador tiene poco tiempo jugado, puede que vengas de un servidor instan","error");
						else if(val == -5)
							$.notify("Tu archivo es incorrecto con la opcion de server disponibles","error");
						else if(val == -6)
							$.notify("No posees cupos para un nuevo personaje","error");
						else if(val == -7)
							$.notify("No pudimos hacer la tranferencia","error");
						else if(val == -8)
							$.notify("Intentas Migrar un personaje ya existente","error");
						else if(val == -9)
							$.notify("Este usuario ya ha sido migrado a este reino","error");
						else if(val == -10)
							$.notify("Error al momento de actualizar el personaje","error");
						else if(val == -11)
							$.notify("El Personaje que desea migrar no coincide con ninguno de tu cuenta","error");

						$("#myModal").modal("toggle");
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						console.log("->"+jqXHR+"->"+textStatus+"->"+errorThrown);
					}
				});
			}
			else{
				alert("el nombre del archivo debe ser zwgamer.lua");
			}
		}
		else{
			alert("debe seleccionar un personaje");
		}
    }

});
$("#loginM").on("submit",function (form) {
   form.preventDefault();
    loader(true,0);
    var usuario = $("#usuario").val();
    var password = $("#password").val();
    $("#preloader").css("display","block");
    $.ajax({
        url : localStorage.url+"migrador/login",
        dataType : 'json',
        type : 'POST',
        data:{
            usuario : usuario,
            password : password
        },
        success: function (data) {
          if(data === true) {
              window.location.href = localStorage.url;
          }
          if(data == false)$.notify("usuario y/o contraseña incorrecta","error");
          if(data == -1){alert("ningun campo puede estar vacio","error");}
          if(data == -2) $.notify("Esta cuente es GM y no tiene permiso para entra","error");
          if(data == -3) $.notify("Tu cuenta no tiene permito ingresar","error");
             loader(0,0);
        }
    });
});
function validarOnline(){
   $.ajax({
        url : localStorage.url+"migrador/validarOnline",
        dataType : 'json',
        type : 'POST',
        success : function (data) {
            localStorage.validarMigration = data;
        }
    });
}
function loader(tiempo,start) {
    if(start == 1){
        var current_progress = 0;
        var interval = setInterval(function() {
            current_progress += 1;
            if(current_progress == 100 ){
                $("#preloader").css("display","none");
                current_progress = 0;
                clearInterval(interval);
            }
        }, tiempo);
    }
    else{
        $("#preloader").css("display","none");
    }
}


function LoaderFile(time) {

    var current_progress = 0;
    var interval = setInterval(function() {
        current_progress += 1;

        $("#dynamic")
            .css("width", current_progress + "%")
            .attr("aria-valuenow", current_progress)
            .text(current_progress + "% Cargando");

        if (current_progress >= time && val == true || val == 1){
            clearInterval(interval);
            current_progress = 0;
            $.notify("Por favor revisa el estado de tu migracion","success");
            clearInterval(interval);
        }
        if(val < 0){
            clearInterval(interval);
            val = false;
        }

    }, time);
}
function loaderVerMigracion(time) {
    var current_progress = 0;
    var interval = setInterval(function() {
        current_progress += 1;
        $("#modalLoader")
            .css("width", current_progress + "%")
            .attr("aria-valuenow", current_progress)
            .text(current_progress + "% Cargado Personaje");

        if(current_progress == time){
            $("#modalLoader").text(current_progress + "% Carga de personaje completa");
            current_progress = 0;
            clearInterval(interval);
        }
    }, time/2);
}
function loaderEnviarItems(tiempo) {
    var current_progress = 0;
    var interval = setInterval(function() {
        current_progress += 1;
        $("#modalLoader1")
            .css("width", current_progress + "%")
            .attr("aria-valuenow", current_progress)
            .text(current_progress + "% Enviado Objetos");
        if(current_progress == tiempo || valItems == true){
            $("#modalLoader1").css("width", tiempo + "%").text(tiempo + "% Objetos Enviados");
            current_progress = 0;
            clearInterval(interval);
        }
    }, tiempo/2);

}
function cargarDatosMigracion() {

    $.ajax({
        url : localStorage.url + "migrador/buscarRealmlist",
        dataType : 'json',
        type: 'POST',
        success : function (data) {
            var string = '';

            for(var i in data){
                string += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
            }
            $("#idRealistm").html(string);
        }
    });


    $.ajax({
        url : localStorage.url + "migrador/buscarServidores",
        dataType : 'json',
        type: 'POST',
        success : function (data) {
            var string = '';
            for(var i in data){
                string += "<option value='"+data[i].id+"'>"+data[i].nombre+"</option>";
            }
            $("#servidorm").html(string);
        }
    });
	
	
	$.ajax({
        url : localStorage.url + "migrador/buscarMisPjs",
        dataType : 'json',
        type: 'POST',
        success : function (data) {
            var string = "<option>Seleccione el Personaje a migrar</option>";
            for(var i in data){
                string += "<option value='"+data[i].guid+"'>"+data[i].name+"</option>";
            }
            $("#guid").html(string);
        }
    });

}

$('#guid').on('change', function (e) {
	alert("recuerda que el que selecciones debe coincidir con el characters a migrar en los siguientes aspectos: SEXO, RAZA, CLASE");
})

function completarMigracion() {
    if(confirm("Desea Completar la Migracion de este personaje?")){
        $.ajax({
            url: localStorage.url + "migrador/completarMigracion",
            dataType: 'json',
            type: 'POST',
            data: {
                id: $("#character").val()
            },
            success: function (data) {
                if(data == -1)
                    $.notify("Tu no puede hacer esto","error");

                else if(data == true)
                    $.notify("Accion Completada Con éxito","succes");

                else if(data == false)
                    $.notify("Algo Salio mal","error");
                else if(data == -2)
                    $.notify("Tu migracion no a sido aceptada","error");
                else
                    $.notify("Ocurrio algun Error","error");
            }
        });
    }


}
function buscarMigraciones() {
    $.ajax({
        url : localStorage.url + "migrador/buscarMigraciones",
        dataType : 'json',
        type: 'POST',
        success : function (data) {
            var string = '';
            for(var i in data){
                string += "<option value='"+data[i].guid+"'>"+data[i].nombreNuevo+"</option>";
            }
            Data = data;
            $("#character").html(string);
        }
    });
}
function EliminarMigracion(id) {

    if(confirm("Desea Eliminar esta Migracion?")){
        $.ajax({
            url: localStorage.url + "migrador/eliminarMigracion",
            dataType: 'json',
            type: 'POST',
            data: {
                id: $("#character").val()
            },
            success: function (data) {
                if(data == -1)
                    $.notify("Tu no puede hacer esto","error");

                if(data == true)
                    $.notify("Accion Completada Con éxito","succes");

                if(data == false)
                    $.notify("Algo Salio mal","error");

            }
        });
    }

}
function activarEnvioObjetos() {
    if(confirm("Para Enviar los objetos a tu Personaje este debe estar en linea, ¿Desea Enviar los Objetos?")) {
        $("#view-modal").modal("toggle");
        $("#btnL").css("background","linear-gradient(rgba(0,0,0,.8) 0%, rgb(32, 30, 28) 100%)");
        $("#btnL").css("color","#ffffffcc");
        $("#aceptarTerminos").css("background","rgba(255, 255, 255, 0.35)")
        $("#enLinea").modal({backdrop: 'static', keyboard: false});
    }
}
function enviarObjetos(guidPj) {
    if(guidPj.length>0) {
        if($("#nombrePj").val().length>0){
            $.ajax({
                url: localStorage.url + "migrador/validarJugadorOnline",
                dataType: 'json',
                type: 'POST',
                data: {
                    guid: $("#character").val()
                },
                success: function (data) {
                    if(data == true || data == 1){
                        loaderEnviarItems(200);
                        $.ajax({
                            url: localStorage.url + "migrador/enviarItems",
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                id: guidPj
                            },
                            success: function (data1) {
                                if (data1 == -1){
                                    $.notify("Tu no puede hacer esto", "error");
                                }
                                else if (data1 == true || data1 == 1){
                                    $.notify("Accion Completada Con éxito", "succes");
                                    valItems = true;
                                }
                                else if (data1 == false){
                                    $.notify("Algo Salio mal", "error");
                                }
								else if (data1 == -2){
									$.notify("Este personaje no puede recibir mas de 1 envio de objetos", "error");
								}
								else if(data1 == -3){
									$.notify("Este personaje debe estar online", "error");

								}
                                $("#btNCerrar").prop('disabled', false);
                                $("#btnL").prop('disabled', false);

                            }
                        });
                    }
                    if(data == 2){
                        $("#btNCerrar").prop('disabled', false);
                        $("#btnL").prop('disabled', false);
                        $.notify("Tienes Pendiente Un cambio de nombre, por favor entra y cambialo","warning");
                    }else if(data == -1){
						$("#btNCerrar").prop('disabled', false);
                        $("#btnL").prop('disabled', false);
                        $.notify("Personaje No se encuentra Online","warning");
                        $.notify("Personaje No se encuentra Online","warning");
					}
                }
            });
        }
    }
}