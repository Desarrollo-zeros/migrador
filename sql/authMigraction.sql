alter table realmlist add migracion TINYINT;

DROP TABLE IF EXISTS `servidoresAceptado`;
CREATE TABLE servidoresAceptado(
	id TINYINT NOT NULL PRIMARY KEY,
	nombre VARCHAR(20) NOT NULL,
	dominio VARCHAR(100) NOT NULL,
	estado TINYINT DEFAULT 1
);

DROP TABLE IF EXISTS `cuentasTransferidas`;
CREATE TABLE cuentasTransferidas(
	id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'identificacion' PRIMARY KEY,
	guid INT(11) NOT NULL UNIQUE,
	account VARCHAR(20) NOT NULL,
	nombreViejo VARCHAR(20) NOT NULL,
	nombreNuevo VARCHAR(20) NOT NULL,
	fechaCreacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	usuarioViejo VARCHAR(16) NOT NULL DEFAULT '',
	contraseñaVieja VARCHAR(40) NOT NULL DEFAULT '',
	nombreRealmVieja VARCHAR(255) NOT NULL,
	realmlistViejo VARCHAR(255) NOT NULL,
	items MEDIUMTEXT,
	dump MEDIUMTEXT,   
	gmAccount INT(11) NOT NULL,
	estado TINYINT DEFAULT 0,
	idReamlist TINYINT NOT NULL
);

DROP TABLE IF EXISTS `itemsAceptados`;
CREATE TABLE itemsAceptados(
	id INT(10) PRIMARY KEY,
	nuevoID INT(10),
	nombre varchar(20) NOT NULL,
	link text
);


