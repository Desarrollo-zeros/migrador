<?php
/**
 * Created by PhpStorm.
 * User: zeros
 * Date: 4/02/2018
 * Time: 4:16 PM
 */

class Migrador extends CI_Controller{

    private $uploads = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Auth");
        $this->load->model("Characters");
        $this->load->model("World");

        $this->uploads['upload_path'] = 'uploads/';
        $this->uploads['allowed_types'] = '*';
        $this->uploads['max_filename'] = '255';
        $this->uploads['encrypt_name'] = FALSE;
        $this->uploads['max_size'] = '1024'; //1 MB
        set_error_handler(array(&$this,"catchError"));
    }


    function catchError($errno, $errstr){
        return true;
    }

    public function index(){
        $this->load->view("migrador");
    }

    public function login(){
        if(!isset($_POST)){
            redirect("");
        }
        if(empty($this->input->post("usuario")) && empty($this->input->post("password"))){
            echo json_encode(-1);
        }
        $usuario =  $this->input->post("usuario");
        $password = $this->Auth->encryptAccount($this->input->post("usuario"),$this->input->post("password"));

        if($this->Auth->validarLogin($usuario,$password)){
            echo json_encode($this->Auth->validarIsGm());
        }
        else{
            echo json_encode(false);
        }
    }

    public function validarOnline(){
        if(isset($this->session->id)) echo json_encode(true);
    }

    public function recibirArchivoMigracion(){
		if(isset($this->session->id)){
			if($this->World->validarServidorOffline($this->input->post("idRealistm"),$this->Auth->loaderRealmlist())){
				if (isset($_FILES['fileMigrador']['name'])) {
					if (1 > $_FILES['fileMigrador']['error']) {
						$this->load->library("upload",$this->uploads);
						if($this->upload->do_upload('fileMigrador')){
							$this->Characters->migracion["usuario"] = !$this->config->item("migracionDirecta") ?  $this->input->post("usuariom") : "no";
							$this->Characters->migracion["password"] = !$this->config->item("migracionDirecta") ?  $this->input->post("passwordm") : "no";
							$this->Characters->migracion["idReamlist"] = $this->input->post("servidorm");
							$this->Characters->migracion["guid"] = $this->input->post("guid");
							echo json_encode($this->Characters->obtenerDatosLua());
						}
					}
				}
			}
			else{
				echo json_encode(-1);
			}
		}
		else{
			echo json_encode(-1);
		}
    }

    public function buscarRealmlist(){
		if(isset($this->session->id)){
			echo json_encode($this->Auth->loaderRealmlist());
		}
    }

    public function buscarServidores(){
		if(isset($this->session->id)){
			echo json_encode($this->Auth->loaderServidores());
		}
    }

    public function buscarPersonajes(){
		if(isset($this->session->id)){
			if(!empty($this->input->post("id"))){
				echo json_encode($this->Characters->loaderCharacters($this->input->post("id")));
			}
		}
    }

    public function buscarMigraciones(){
		if(isset($this->session->id)){
			echo json_encode($this->Auth->loaderMigraciones());
		}
    }


    public function completarMigracion(){
		if(isset($this->session->id)){
			if(!$this->session->isMaster &&  !empty($this->input->post("id"))){
				echo json_encode(-1);
			}
			else{
				if(!$this->Auth->validarEstadoMigracion($this->input->post("id"))){
					echo json_encode($this->Auth->completarMigracion($this->input->post("id")));
				}
				else{

				}
			}
		}
    }

    function eliminarMigracion(){
		if(isset($this->session->id)){
			if($this->session->isMaster  && !empty($this->input->post("id"))){
				echo json_encode($this->Auth->eliminarMigracion($this->input->post("id")));
			}else{
				echo json_encode(-1);
			}
		}
    }

    function enviarItems(){
        if(isset($this->session->id)  && !empty($this->input->post("id"))){ //bien
            if($this->World->validarServidorOffline($this->config->item("idRealmist"),$this->Auth->loaderRealmlist())){ //bien
				if(!$this->Auth->validaMigracionCompleta($this->input->post("id"))){ //bien
					if($this->Characters->validarJugadorOnline($this->input->post("id"),false)){ //bien
						if($this->Auth->validarEstadoMigracion($this->input->post("id")) && !$this->config->item("migracionDirecta") ){ //bien
							echo json_encode($this->Characters->enviarItems($this->Auth->enviarItems($this->input->post("id")),$this->input->post("id")));
						}
						else if($this->config->item("migracionDirecta")){
							echo json_encode($this->Characters->enviarItems($this->Auth->enviarItems($this->input->post("id")),$this->input->post("id")));
						}
						else{
							echo json_encode(-2);
						}
					}
					else{
						echo json_encode(-3);
					}
				}
				else{
					echo json_encode(-2);
				}
            }
			else{
				echo json_encode(-1);
			}
        }
        else{
            echo json_encode(-1);
        }
    }

    public function validarJugadorOnline(){
        if(isset($this->session->id)  && !empty($this->input->post("guid"))) {
            echo json_encode($this->Characters->validarJugadorOnline($this->input->post("guid"),false));
        }
    }


    public function salir(){
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        $this->session->sess_destroy();
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
        redirect("");
    }


	public function test(){
       echo ($this->Auth->confirmarCantidadItems(49908,10));
	}

	public function buscarMisPjs(){
		echo json_encode($this->Characters->buscarMisPjs());
	}
}