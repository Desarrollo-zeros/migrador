<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php ob_start('comprimir_pagina'); ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>ZWGamers - Migracion</title>
    <meta name="google-site-verification" content="rkMFFbCodw-MTlIqBhV6rONSf-0ii1A1VYe21vqUZmg" />
    <meta name="author" content="Zeros WoW">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="title" content="ZWGamers - Migracion Pagina.">
    <meta http-equiv="Content-Language" content="en">
    <meta name="Description" content="Bienvenidos a ZWGamers, Servidor privado.">
    <meta name="Keywords" content="ZWGamers, ZWGamers, ZWGamers WoW, WoW, World of Warcraft, Warcraft, wotlk, Wrath of the Lich King, wotlk server, Private Server, Private WoW Server, WoW Server, WoW Private Server, win a character, splash, splash page, reward, rewards">
    <meta name="language" content="English">
    <meta name="type" content="website">
    <meta name="copyright" content="Copyright www.ZWGamers.com">
    <meta name="resource-type" content="games">
    <meta name="Distribution" content="Global">
    <meta name="email" content="webmaster@ZWGamers.com">
    <meta name="Charset" content="UTF-8">
    <meta name="Rating" content="General">
    <meta name="robots" content="INDEX,FOLLOW">
    <meta name="Revisit-after" content="7 Days">
    <meta name="DC.Creator" content="php">
    <meta name="DC.Description" content="Welcome to the best free private server.">
    <meta name="DC.Type" content="text"><meta name="DC.Language" content="en">
    <meta name="DC.Rights" content="(c) ZWGamers.com all rights reserved.">
    <link href="https://zwgamers.com/template/style/images/favicon.ico" rel="icon" type="image/x-icon"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://zwgamers.com/resources/min/?f=template/style/style.css,template/style/technical.css,template/style/select.css,template/style/forms.css,template/style/home.css,template/style/video-js-new-vision.css,template/style/shadowbox.css,template/style/pages-background.css,template/style/bbcode-default.css,template/style/quick-menu.css,template/style/IndexSlider.css,template/style/account_panel.css,template/style/loginbox.css,template/style/alert-box.css,template/style/radio-checkbox.css,template/style/page-store.css" />
	<link rel="stylesheet" href="<?=base_url()?>public/fonts.css"> 
    <link href="<?=base_url()?>public/file/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <script src="<?=base_url()?>public/file/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>public/file/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>public/file/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>public/file/js/fileinput.min.js"></script>
    <script src="<?=base_url()?>public/file/themes/fa/theme.js"></script>
    <script src="<?=base_url()?>public/file/js/locales/es.js"></script>
    <script src="<?=base_url()?>public/file/js/fileinput.js"></script>
    <script src="<?=base_url()?>public/file/themes/gly/theme.js"></script>
    <script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
    <style>
    .progress {
        margin: 10px;
        width: 550px;
    }

    #preloader {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1000
    }

    #preloader #preloader-inner {
        display: block;
        position: relative;
        left: 50%;
        top: 50%;
        width: 150px;
        height: 150px;
        margin: -75px 0 0 -75px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #3498db;
        animation: spin 2s linear infinite
    }

    #preloader #preloader-inner:before {
        content: "";
        position: absolute;
        top: 5px;
        left: 5px;
        right: 5px;
        bottom: 5px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #e74c3c;
        animation: spin 3s linear infinite
    }

    #preloader #preloader-inner:after {
        content: "";
        position: absolute;
        top: 15px;
        left: 15px;
        right: 15px;
        bottom: 15px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #f9c922;
        animation: spin 1.5s linear infinite
    }
    @keyframes
    spin { 0% {
        transform:rotate(0deg)
    }

        to { transform: rotate(1turn) }
    }





    </style>

<body>
<center>


    <!--HEADER-->
    <div id="header" align="center">
        <div class="holder">

            <a href="./" class="logo"><p></p></a>

            <ul class="top-navigation">
                <li><a id="home" href="https://zwgamers.com/index.php?page=home"><p></p></a></li>
                <li><a id="forums" href="https://zwgamers.com/forums.php"><p></p></a></li>
                <li id="support">
                    <p></p>
                    <div class="nddm-holder" align="center">
                        <div class="navi-dropdown">
                            <p id="arrow"></p>
                            <!--<span><a href="https://zwgamers.com/index.php?page=addons">Addons</a></span>-->
                            <span><a href="https://zwgamers.com/index.php?page=howto">Como?</a></span>
                            <span><a href="https://zwgamers.com/forums/viewforum.php?f=52">Soporte</a></span>
                            <span><a href="https://zwgamers.com/index.php?page=terms-of-use">Términos de Uso</a></span>
                            <span><a href="https://zwgamers.com/index.php?page=references">Referencias</a></span>
                            <span><a href="https://zwgamers.com/index.php?page=rules">Reglas</a></span>
                        </div>
                    </div>
                </li>
                <li>
                    <a id="features" href="https://zwgamers.com/index.php?page=features"><p></p></a>
                    <p></p>
                    <div class="nddm-holder features" align="center">
                        <div class="navi-dropdown">
                            <p id="arrow"></p>
                            <span><a href="https://zwgamers.com/index.php?page=downloads">Descargas</a></span>
                            <span><a href="https://zwgamers.com/index.php?page=bugtracker">Bug Tracker</a></span>
                            <span><a href="https://zwgamers.com/index.php?page=changelogs">Changelogs</a></span>
                            <span><a href="https://zwgamers.com/index.php?page=working_content">Contenido del Servidor</a></span>
                        </div>
                    </div>
                </li>
                <li>
                    <a id="media" href="https://zwgamers.com/index.php?page=media"><p></p></a>
                    <p></p>
                    <div class="nddm-holder media" align="center">
                        <div class="navi-dropdown">
                            <p id="arrow"></p>
                            <span><a href="https://zwgamers.com/index.php?page=all-wallpapers">Wallpapers</a></span>
                            <span><a href="https://zwgamers.com/index.php?page=all-videos">Videos</a></span>
                            <span><a href="https://zwgamers.com/index.php?page=all-screenshots">Screenshots</a></span>
                        </div>
                    </div>
                </li>

                <li><a id="goal" href="https://zwgamers.com/index.php?page=armory"><p></p></a></li>
            </ul>

        </div>
    </div>
    <!--HEADER.End-->

    <div id="image_header" align="center">
        <div class="sub_head_image" align="center">
        </div>
    </div>

    <!-- BODY-->
    <div class="main_b_holder" align="center">

        <div class="space-fix"></div>
        <div class="sec_b_holder" align="center">
            <div id="body" align="left">
                <!-- BODY Content start here -->
                <script type="text/javascript">
                    document.oncontextmenu = function(){return false;}
                </script>
                <div class="content_holder">

                    <div class="sub-page-title">
                        <div id="title"><h1>Migrador<p></p><span></span></h1></div>
                    </div>

                    <div class="container_2 account" align="center">
                        <div class="cont-image">

                            <div class="container_3 account_sub_header">
                                <div class="grad">
                                    <div class="page-title">Migrador ZWGamers</div>
                                    <?php if(isset($this->session->id)){?>
                                    <a href="<?=base_url("migrador/salir")?>">Salir</a>
                                    <?php }?>
                                </div>
                            </div>

                            <div class="store_notice">
                                <h1></h1>
                            </div>
                            <?php if(!isset($this->session->id)){?>
                                <div class="items-currency-list" id="iniciarSession">
                                    <div id="store-complete-form-container" class="container_3 account-wide" style="height:290px;">
                                        <form method="post" id="loginM">
                                            <div class="form-group" style="position: relative; right: 250px;">
                                                <br>
                                                <input id="usuario" type="text" class="form-control" name="usuario" placeholder="Usuario" required>
                                                <br>
                                                <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>
                                                <input type="submit" class="form-control" value="Iniciar Session" style="position: relative; right: 252px; width: 300px;"/>
                                            </div>
                                        </form>
                                        <img src="https://zwgamers.com/img/migration.png" width="500"  style="position: relative; left: 335px; top: -113px;">
                                    </div>
                                </div>
                            <?php }?>

                            <?php if(isset($this->session->id) && (!$this->session->isMaster)){?>
                                <div class="items-currency-list" id="panelM">
                                    <div id="store-complete-form-container" class="container_3 account-wide" style="height:500px;">
                                        <input type="submit" class="form-control"  onclick="parent.open('<?=base_url("zwgamers.rar")?>')" value="Descargar Addon" style="width: 240px;"/>
                                        <input type="submit" id="migrarp" class="form-control"  value="Migrar Personaje" style="width: 240px;"/>
                                        <input type="submit" id="verp" class="form-control"  value="ver estado migratorio" style="width: 240px;"/>
										
										<div id="verVideo" style="position:relative; top:40px;">
											<p>Como Migrar</p>
											<iframe width="560" height="325" src="https://www.youtube.com/embed/8cCcEQFop-I?rel=0&amp;showinfo=0" frameborder="0" encrypted-media" allowfullscreen></iframe>
										</div>
										<br><br>
										 <input type="submit" class="form-control"  onclick="parent.open('https://mega.nz/#!gB8DXC6B!2if_PCIrqjlrb4upJk365YfgBajcSxXSCEIDW9D_Wm4')" value="segunda opcion de descarga addon mega" style="width: 350px;"/>
										 <input type="submit" class="form-control"  onclick="parent.open('http://www.mediafire.com/file/cq5u465orcueaof/zwgamers.rar')" value="segunda opcion de descarga addon mediafire" style="width: 350px;"/>

										

                                        <div id="verPersonajeMigrados" style="position: relative; top:40px; display: none;">

                                            <form id="mostarPersonaje">
                                                <div class="col-md-12">
                                                    <select  class="form-control" id="character" name="character">
                                                        <option >Seleccione un personaje</option>
                                                    </select>
                                                </div>

                                                <input id="estadoInput" type="hidden">

                                                <input type="submit" class="form-control"  value="Mostrar Informacion"/>
                                            </form>

                                        </div>

                                        <div id="migrarPersonaje" style="position: relative; left: 20px; top:10px; display: none;">
                                            <form class="col-md-7" id="enviarFile">
                                                <input type="hidden" id="eval" name="eval" value="1">

                                                <div class="form-group" style="position: relative; right: 85px;">
                                                    <br>
                                                  
													<?php if(!$this->config->item("migracionDirecta")){?>
													<p class="select-charcater-selected">Datos de tu servidor antiguo</p>
                                                    <br>
                                                    <input id="usuariom" type="text" class="form-control" name="usuariom" placeholder="Usuario" required>
                                                    <br>
                                                    <input id="passwordm" type="password" class="form-control" name="passwordm" placeholder="Contraseña" required>
                                                    <br>
													<?php }?>
													<p style="position:relative; right: 95px;">Servidor de origen</p>
                                                    <select id="servidorm" name="servidorm" class="form-control" required style="position: relative; left: 85px;">
                                                        <option>Seleccione servidor</option>
                                                    </select>

                                                    <br>
													<p style="position:relative; right: 98px;">Reino A migrar</p>
                                                    <select id="idRealistm" name="idRealistm" class="form-control" required style="position: relative; left: 85px;">
                                                        <option>Seleccione un reino a migrar</option>
                                                    </select>
													
													<br>
													<p style="position:relative; right: 98px;">Characters</p>
                                                    <select id="guid" name="guid" class="form-control" required style="position: relative; left: 85px;">
                                                        <option>Seleccione un reino a migrar</option>
                                                    </select>
													
                                                </div>

                                                <div class="file-loading">
                                                    <input id="fileMigrador" name="fileMigrador" type="file"  class="file" accept=".lua" data-show-preview="false"   data-show-upload="false" required>
                                                </div>
                                                <input type="submit" class="form-control"  value="Enviar Archivo" style="position: relative; left: 278px; bottom:50px; width: 240px;"/>
                                            </form>


                                            <form>

                                            </form>



                                        </div>
                                    </div>
                                </div>
                            <?php }?>



                            <?php if(isset($this->session->id) && ($this->session->isMaster)){?>
                                <div class="items-currency-list">
                                    <div id="store-complete-form-container" class="container_3 account-wide" style="height:400px;">

                                        <form id="mostarPersonaje">
                                            <br><br><div class="col-md-12">
                                                <select  class="form-control" id="character" name="character">
                                                    <option >Seleccione un personaje a conocer el estado</option>
                                                </select>
                                            </div>
                                            <input type="hidden" id="isMaster" value="<?=$this->session->isMaster?>">
                                            <input type="submit" class="form-control"  value="Mostrar Informacion"/>

                                        </form>

                                    </div>
                                </div>
                            <?php }?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- BODY Content end here -->
        </div>
    </div>

    <div class="footer-holder">
        <div id="footer">
            <a class="back-to-top" href="#header"></a>

            <div class="left-side">
                <a class="evil-logo" href="https://vps-plus.online/" target="_blank">Powered By VPSplus</a>
                <a class="dmca" href="https://www.dmca.com/Protection/Status.aspx?ID=cf56126a-7e17-426c-b7b5-aa7668c01834" title="DMCA"> <img src ="https://zwgamers.com/template/style/images/dmca_protected_sml_120x.png" title="DMCA" target="_blank"><img alt="DMCA.com" />Protected by DMCA</a>
                <p>
                    Todos los derechos reservados ® <b>ZWGamers</b> World Of Warcraft <br/>
                    Esta web y contenido fueron creados para <i>ZWGamers</i>.
                </p>
            </div>

            <div class="right-side">
                <ul>
                    <li><a href="https://zwgamers.com/index.php?page=all-wallpapers">Wallpapers</a></li>
                    <li><a href="https://zwgamers.com/index.php?page=all-videos">Videos</a></li>
                    <li><a href="https://zwgamers.com/index.php?page=all-screenshots">Screenshots</a></li>
                </ul>

                <ul>
                    <li><a href="https://zwgamers.com/index.php?page=references">Referencias</a></li>
                    <li><a href="https://zwgamers.com/index.php?page=rules">Reglas</a></li>
                    <li><a href="https://zwgamers.com/index.php?page=terms-of-use">Terminos de uso</a></li>
                    <li><a href="https://zwgamers.com/index.php?page=howto">Como?</a></li>
                </ul>

                <ul>
                    <li><a href="https://zwgamers.com/index.php">Inicio</a></li>
                    <li><a href="https://zwgamers.com/index.php?page=changelogs">Changelog</a></li>
                    <li><a href="https://zwgamers.com/forums/">Foros</a></li>
                    <li><a href="https://zwgamers.com/index.php?page=home">Armeria</a></li>
                </ul>
            </div>

        </div>
        <div class="bot-foot-border"></div>
        <div class="bot-foot-glow"></div>
    </div>
</center>

<div id="temp-login-form" style="display: none;">
    <div class="login-box" align="left">
        <form action="https://zwgamers.com/execute.php?take=login" method="post">
            <input type="hidden" name="url_bl" id="js-login-box_urlbl" />
            <p>Id de Cuenta</p>
            <input type="text" name="username" autocomplete="on"> <br>
            <p>Password</p>
            <input type="password" name="password" autocomplete="on"><br>
            <div class="login-box-row">
                <input type="submit" value="Login">
                <label class="label_check">
                    <div></div>
                    <input type="checkbox" value="1" id="rememberme" name="rememberme">
                    <p>Recordar Cuenta</p>
                </label>
            </div>
        </form>
        <div class="login-box-options">
            <a href="https://zwgamers.com/index.php?page=password_recovery">Perdiste tu Password ?</a><br>
            <span>Aún no tienes una cuenta ? <a href="https://zwgamers.com/index.php?page=register">Registrar Ahora!</a></span>
        </div>
    </div>
</div>


<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-clock-o"></i>Estamos Trabajando, nos tardaremos maximo 2 minutos</h4>
            </div>
            <div class="modal-body center-block">
                <div class="progress">
                    <div id="dynamic" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        <span id="current-progress"></span>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<div id="enLinea" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    <i class="glyphicon glyphicon-user"></i> Personaje Migrado
                </h4>
            </div>
            <div class="modal-body">
               
                <div id="dynamic-content"> <!-- mysql data will load in table -->
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Terminos y condiciones</h1>
                            <p>1: Para enviar los objetos a tu personaje este {<label style="color: red; font-size: 15px;">ojo tu personaje debe estar online).</label>}</p>
                            <p>2: los Objetos pueden tardar de llegar a tu buzon, por favor espera.</p>
                            <p>3: No respondemos por fallas del Usuario.</p>
							<p>4: la primera ves que das acepto terminos y condiciones, este te pondra un cambio de nombre pendiente y te sacara del juego para que hagas el cambio de nombre y preciona click de nuevo asi se te enviaran los objetos a tu buzon. </p>
                            <br><br>
                                <form id="enLineaFrom">
                                    <label>Debes Escribir La Palabara {"enviar"}</label>
                                    <input type="text" class="form-control" id="aceptarTerminos" name="aceptarTerminos" placeholder="enviar" required>
                                    <input type="hidden" id="guidPj" name="guidPj">
                                    <input id="btnL" type="submit" class="btn btn-success" value="Acepto terminos y condiciones" style="position: relative; bottom: 33px; left: 300px; height: 30px;">
                                </form>

                            <div class="progress">
                                <div id="modalLoader1" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btNCerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>




<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    <i class="glyphicon glyphicon-user"></i> Personaje Migrado
                </h4>
            </div>
            <div class="modal-body">
                
                <div id="dynamic-content"> <!-- mysql data will load in table -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <th>Nombre Pj</th>
                                        <input type="hidden" id="nombrePj">
                                        <td><p id="nombreP" align="center" class="center-block" style="color: #000000; font-size: 15px;"></p></td>
                                    </tr>

                                    <tr>
                                        <th>Nivel</th>
                                        <td><p id="nivelPj" align="center" class="center-block" style="color: #000000;; font-size: 15px;"></p></td>
                                    </tr>

                                    <tr>
                                        <th>Clase</th>
                                        <td><img id="clasesPj" width="30" align="center" class="center-block"></td>
                                    </tr>

                                    <tr>
                                        <th>Raza</th>
                                        <td><img id="racesPj" width="30" align="center" class="center-block"></td>
                                    </tr>

                                    <tr>
                                        <th>Estado De Migracion</th>
                                        <td><div id="estadoPj" align="center" class="center-block"></div></td>
                                    </tr>

                                    <tr>
                                        <th>Recibir Mis Items</th>
                                        <td><div id="RecibirItems" align="center" class="center-block"></div></td>
                                    </tr>
                                    <?php if(isset($this->session->id) && ($this->session->isMaster)){?>
                                    <tr>
                                        <th>Eliminar Migracion</th>
                                        <td><div id="EliminarMigracion" align="center" class="center-block"></div></td>
                                    </tr>
                                    <?php }?>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="progress">
                <div id="modalLoader" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


</body>



<div id="preloader" style="display: none">
    <div id="preloader-inner"></div>
</div>

<script src="<?=base_url()?>public/core.js"></script>
<script src="<?=base_url()?>public/loader.js"></script>


<?php
// Una vez que el bÃºfer almacena nuestro contenido utilizamos "ob_end_flush" para usarlo y deshabilitar el bÃºfer
ob_end_flush();
// FunciÃ³n para eliminar todos los espacios en blanco
function comprimir_pagina($buffer) {
    $busca = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
    $reemplaza = array('>','<','\\1');
    return preg_replace($busca, $reemplaza, $buffer);
}?>

<?php if(isset($this->session->id)){?>
    <script>
        $(document).ready(function () {
            $("#preloader").css("display","block");
            loader(20,1);
        });
    </script>
<?php }?>

<script>
    (function(d, w, c) {
        w.ChatraID = '2LzgYsQDAMBo6GGLe';
        var s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
            + '//call.chatra.io/chatra.js';
        if (d.head) d.head.appendChild(s);
    })(document, window, 'Chatra');
</script>
<script>
    $(document).ready(function () {
        localStorage.url = "<?=base_url()?>";
		localStorage.migracionDirecta = "<?=json_encode($this->config->item("migracionDirecta"));?>";
    });
</script>
<script>
    $(document).on('ready', function() {
        $("#fileMigrador").fileinput({
            previewFileType: "lua",
            allowedFileExtensions: ["lua"],
            previewClass: "bg-warning",
            theme: "gly",
            uploadUrl: "/file-upload-batch/2",
            hideThumbnailContent: true // hide image, pdf, text or other content in the thumbnail preview
        });
    });
</script>



</html>
