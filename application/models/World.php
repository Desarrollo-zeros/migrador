<?php
/**
 * Created by PhpStorm.
 * User: zeros
 * Date: 5/02/2018
 * Time: 12:26 PM
 */

class World extends CI_Model{

    private $db = NULL;

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('world', TRUE);
    }


    function validarServidorOffline($id,$data = array()) {
            foreach ($data as $row) {
                if ($id == $row->id) {
                    $ip = $row->address;
                    $port = $row->port;
                    $fp = fsockopen($ip, $port, $errno, $errstr, 30);
                    $val = $fp ? true : false;
                    fclose($fp);
                }
            }
            unset($fp);
            return $val;
    }

    function itemsExisteEnDb($item){
       return $this->db->query("SELECT entry FROM item_template where entry = {$item} ")->num_rows() > 0 ? true : false;
    }


}