<?php
/**
 * Created by PhpStorm.
 * User: zeros
 * Date: 5/02/2018
 * Time: 12:50 AM
 */

class Characters extends CI_Model{


    /*step 1*/
    private $db = null;
    private $archivoLua = "uploads/zwgamers.lua";
    private $abrirArchivLua = "";
    private $varlorEnBuffer1  = "";
    private $varlorEnBuffer2 = "";
    private $datosEnParte =  null;
    private $switch = null;
    public $migracion = array();

    /*step 1*/

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('characters', TRUE);
        include_once ("application/libraries/switchs.php");
        $this->load->model("Auth");
        $this->switch =  new switchs();
        ini_set('max_execution_time', 1000);

        $this->client = new SoapClient(NULL, array(
                "location"      => $this->config->item("hostSoap").":".$this->config->item("portSoap"),
                'uri'      => 'urn:TC',
                "user_agent"    => "aframework",
                "style"         => SOAP_RPC,
                "login"         => $this->config->item("accountGmMigracion"),
                "password"      => $this->config->item("passwordGMMigracion"),
                "trace"         => 1,
                "exceptions"    => 0
            )
        );



    }

    public function obtenerDatosLua(){
        /*Abro el archivo*/
        $this->abrirArchivLua = fopen($this->archivoLua,"r");
        /*Recorro el archivo y lo almaceno en una nueva variable*/
        while(!feof($this->abrirArchivLua)){
            $this->varlorEnBuffer2 = fgets($this->abrirArchivLua);
            $this->varlorEnBuffer1 .= $this->varlorEnBuffer2;
        }
        /*cierro el archivo*/
         fclose($this->abrirArchivLua);
        /*Elimino el archivo*/
         unlink($this->archivoLua);

        //deslimito espacios y paso a un vector los datos
        $this->datosEnParte       = explode('"', $this->varlorEnBuffer1);

        //pregunto si existe datos
        if(isset($this->datosEnParte[1])){
            $this->migracion["dump"] = $this->datosEnParte[1];
            if($this->Auth->validarDump($this->migracion["dump"])){
                return -8; //dump ya existe
            }
            $this->migracion["dumpDesencriptado"] = $this->desecriptarJSON($this->migracion["dump"]);
            $this->migracion["idAccount"] = $this->session->id;
            $this->migracion["idGmMigracion"] = $this->config->item('idGmMigracion');
            $this->migracion["json"] = json_decode(stripslashes($this->migracion["dumpDesencriptado"]), true);
            $this->migracion["characterNombre"] = mb_convert_case(mb_strtolower($this->migracion["json"]['uinf']['name'], 'UTF-8'), MB_CASE_TITLE, 'UTF-8');
            $this->migracion["realmlistViejo"] = $this->migracion["json"]['ginf']['realmlist'];

            if($this->Auth->validarExisteMigracion($this->migracion["characterNombre"],$this->migracion["realmlistViejo"])){
                return -9; // ;
            }
            $this->migracion["nombreRealmViejo"] = $this->migracion["json"]['ginf']['realm'];
            $this->migracion["raza"] = $this->switch->obtenerRaceID(strtoupper($this->migracion["json"]['uinf']['race']));
            $this->migracion["clase"] = $this->switch->obtenerClassID(strtoupper($this->migracion["json"]['uinf']['class']));
            $this->migracion["level"] =  $this->migracion["json"]['uinf']['level'] > $this->config->item('levelMaximo') ? $this->config->item('levelMaximo') : $this->migracion["json"]['uinf']['level'];
            $this->migracion["money"] =  $this->migracion["json"]['uinf']['money'] > $this->config->item("maximoOro") ? $this->config->item("maximoOro") :  $this->migracion["json"]['uinf']['money'];
			$this->migracion["talentos"] = $this->migracion["json"]['uinf']['specs'];
			if($this->migracion["talentos"] == 2){
					$this->migracion["money"] = $this->migracion["money"] + 20000000;   
			}
            $this->migracion["sexo"] = $this->migracion["json"]['uinf']['gender'] - 2 == 1 ? 1 : 0;
            $this->migracion["muertes"] = $this->migracion["json"]['uinf']['kills'];
            $this->migracion["arenas"] = $this->migracion["json"]['uinf']['arenapoints']  > $this->config->item("maximoArena")  ? $this->config->item("maximoArena") : $this->migracion["json"]['uinf']['arenapoints'];
            $this->migracion["honor"] = $this->migracion["json"]['uinf']['honor']  > $this->config->item("maximoHornor")  ? $this->config->item("maximoHornor") : $this->migracion["json"]['uinf']['honor'];
            $this->migracion["locales"]  = trim(strtoupper($this->migracion["json"]['ginf']['locale']));

            $this->migracion["contadorLogros"] = 0;
            $this->migracion["tiempoMinLogros"] = 0;
            $this->migracion["tiempoMaximoLogro"] = 0;


            foreach($this->migracion["json"]["achiev"] as $key => $value) {
                $this->migracion["tiempoMinLogros"] = $this->migracion["tiempoMinLogros"] == 0 ? $value['D'] : NULL;
                $this->migracion["tiempoMinLogros"] = $value['D'] > 0 ?  $value['D'] : NULL;
                $this->migracion["tiempoMaximoLogro"] = $value['D'] < 0 ?  $value['D'] : NULL;
                ++$this->migracion["contadorLogros"];
            }

            if($this->config->item("gameBuild") != $this->migracion["json"]['ginf']['clientbuild']){
                return $Retornar = -2; //gameBuild Error
            }
            if($this->migracion["contadorLogros"] < $this->config->item("MinimoTotalLogros")){
                return $Retornar = -3; //logros minimos
            }
            if($this->switch->chekearDiasJugados($this->migracion["tiempoMaximoLogro"],$this->migracion["tiempoMaximoLogro"]) > $this->config->item("tiempoJugado")){
                return $Retornar = -4; //Poco tiempo Jugado
            }

            if(!$this->Auth->validarServer($this->migracion["realmlistViejo"])){
                return -5; //
            }

            if($this->validarMaxPjPorCuenta() >9 ){
                return -6; //maximo pj por cuenta
            }


            
            $this->migracion["itemMasCantidad"] = "";
            $this->migracion["gemas"] = "";
            $this->migracion["monedas"] = "";

            foreach($this->migracion["json"]['currency'] as $key => $value) {
                $CurrencyID = $value['I'];
                $cantidad      = $value['C'];
                if($cantidad < 1)
                    continue;
                if(!in_array($CurrencyID, $this->config->item("monedasWoW")) && $this->World->itemsExisteEnDb($CurrencyID))
					 $cantidad = $this->Auth->confirmarCantidadItems($CurrencyID,$cantidad);
                    $this->migracion["monedas"] .= $CurrencyID.":".$cantidad." ";
            }


            foreach($this->migracion["json"]['inventory'] as $key => $value) {
                $item = $this->Auth->cambioItems($value['I']);
                if($value['C'] < 1 || $value['C'] == 0){
                    $count = 1;
                }
                else if($value['C'] > 1000){
                    $count = 1000;
                }
                else if($value['C'] < 1000 && $value['C'] > 100){
                    $count = 100;
                }
                else{
                    $count = $value['C'];
                }

                if(!in_array($item,$this->config->item("monedasWoW")) && $this->World->itemsExisteEnDb($item)){
                    $cantidad = $count == 0 ? 1 : $count;
                    $cantidad = $this->Auth->confirmarCantidadItems($item,$cantidad);
                    if($cantidad >20 && $cantidad < 200){
                       for($i = 0; $i<round($cantidad/20);$i++){
                           $this->migracion["itemMasCantidad"] .= $item.":"."20 ";
                       }
                    }
                    else{
                        $this->migracion["itemMasCantidad"] .= $item.":".$cantidad." ";
                    }
                }

                $GEM1   = $this->switch->obtenerGemID($value['G1']);
                $GEM2   = $this->switch->obtenerGemID($value['G2']);
                $GEM3   = $this->switch->obtenerGemID($value['G3']);
                if($GEM1 > 1)
                    $this->migracion["gemas"] .= $GEM1 .":1 ";
                if($GEM2 > 1)
                    $this->migracion["gemas"] .= $GEM2 .":1 ";
                if($GEM3 > 1)
                    $this->migracion["gemas"] .= $GEM3 .":1 ";
            }

            $this->migracion["items"] = trim( $this->migracion["itemMasCantidad"] . $this->migracion["gemas"] . $this->migracion["monedas"]);

			if($this->validarIsPj($this->migracion["guid"],$this->migracion["raza"],$this->migracion["clase"],$this->migracion["sexo"])){
				
				$this->validarJugadorOnline($this->migracion["guid"],true);
				$data = array(
					"guid" => $this->migracion["guid"],
					"account" => $this->session->id,
					"nombreViejo" =>$this->migracion["characterNombre"],
					"nombreNuevo" => !$this->buscarExisteNombre() ? 'm'.$this->migracion["characterNombre"].'i' : $this->migracion["characterNombre"],
					"usuarioViejo" => $this->migracion["usuario"],
					"contraseñaVieja" => $this->migracion["password"],
					"nombreRealmVieja" => $this->migracion["nombreRealmViejo"],
					"realmlistViejo" => $this->migracion["realmlistViejo"],
					"items" =>  $this->migracion["items"],
					"dump" => $this->migracion["dump"],
					"gmAccount" => $this->migracion["idGmMigracion"],
					"estado" => (int)2, //1 completo, 2 en espera, 3 eliminado, -1 error
					"idReamlist" => $this->migracion["idReamlist"] //id
				);

				$this->migracion["characterNombre"] = !$this->buscarExisteNombre() ? 'm'.$this->migracion["characterNombre"].'i' : $this->migracion["characterNombre"];


				if(!$this->Auth->insert("cuentasTransferidas",$data)){
					return -7; //error a ingresar datos de la transferencia
				}

				unset($data);
				$this->validarJugadorOnline($this->migracion["guid"],true);
				$data = array(
					//"guid" => $this->migracion["guid"],
					//"account" => $this->session->id,
					//"name" => $this->migracion["characterNombre"],
					"race" => $this->migracion["raza"],
					"class" => $this->migracion["clase"],
					"gender" => $this->migracion["sexo"],
					"level" => $this->migracion["level"],
					"money" => (int)$this->migracion["money"],
					"totalHonorPoints" => $this->migracion["honor"],
					"arenaPoints" => $this->migracion["arenas"],
					"totalKills" =>  $this->migracion["muertes"],
					"taximask" => "0 0 0 0 0 0 0 0 0 0 0 0 0 0",
					"speccount" => 1,
					"position_x"   => (float)5777.18,
					"position_y"   => (float)607.979,
					"position_z"   => (float)565.303,
					"map"          => (int)571,
					"health"       => (int)100,
					"zone"         => (int)4395,
					"cinematic"    => (int)1
				);

				if($this->update("characters","guid",$this->migracion["guid"],$data)){
					if($this->migracion["talentos"] == 2){
					   //$this->aprenderSpell(63644);
					   //$this->aprenderSpell(63645);
					}
					if($this->migracion["clase"] == 6){
						$this->insertarDKSpell();
					}
					$this->insertarGlifos($this->migracion["json"]['glyphs']);
					$this->insertarLogros($this->migracion["json"]['achiev']);
					$this->insertarReputacion($this->migracion["json"]["rep"]);
					$this->insertarSpellCharacters($this->migracion["json"]["skills"]);
					$this->misSpell($this->migracion["json"]["spells"]);
					$this->misMonturas($this->migracion["json"]["creature"]);
					$this->UpdateReputacion($this->migracion["json"]["rep"]);
					return true;
				}
				else{
					return -10; //error al actualizar
				}
			}
			else{
				return -11; //error no validado
			}
        }
    }


    function aprenderSpell($idSpell){
       return $this->insert("character_spell",array("guid" => $this->migracion["guid"],"spell"=>$idSpell));
    }

    function desecriptarJSON($STRING) {
        return strrev(base64_decode(strrev(strrev(base64_decode(strrev($STRING)))))); /*Descripta DUM JSON*/
    }


    function validarMaxPjPorCuenta(){
       return $this->db->query("SELECT COUNT(*) FROM `characters` WHERE `account` = ".$this->session->id." ")->num_rows();
    }

    function obtenerUltimaGuild(){
        return $this->db->query("SELECT MAX(guid) as guid FROM characters")->row()->guid+1;
    }

    function buscarExisteNombre(){
        return $this->db->query("SELECT name FROM characters WHERE name = '".$this->migracion["characterNombre"]."' ")->num_rows() == 0 ? true : false;
    }

    function actualizarNombre(){
        $this->db->where('guid', $this->migracion["guid"]);
        $this->db->update('characters', array("name"=> "m".$this->migracion["characterNombre"]."i"));
    }


    public function insert($tabla,$data = array()){
        return $this->db->insert($tabla,$data);
    }
	
	public function update($tabla,$columa,$id,$data = array()){
		$this->db->where($columa, $id);
		return $this->db->update($tabla,$data);
	}


    public function insertarDKSpell(){
        $data = array(
            array("guid"=>$this->migracion["guid"],"quest"=>12593), array("guid"=>$this->migracion["guid"],"quest"=>12619),
            array("guid"=>$this->migracion["guid"],"quest"=>12641), array("guid"=>$this->migracion["guid"],"quest"=>12657),
            array("guid"=>$this->migracion["guid"],"quest"=>12670), array("guid"=>$this->migracion["guid"],"quest"=>12678),
            array("guid"=>$this->migracion["guid"],"quest"=>12679), array("guid"=>$this->migracion["guid"],"quest"=>12680),
            array("guid"=>$this->migracion["guid"],"quest"=>12687), array("guid"=>$this->migracion["guid"],"quest"=>12697),
            array("guid"=>$this->migracion["guid"],"quest"=>12698), array("guid"=>$this->migracion["guid"],"quest"=>12700),
            array("guid"=>$this->migracion["guid"],"quest"=>12701), array("guid"=>$this->migracion["guid"],"quest"=>12706),
            array("guid"=>$this->migracion["guid"],"quest"=>12711), array("guid"=>$this->migracion["guid"],"quest"=>12714),
            array("guid"=>$this->migracion["guid"],"quest"=>12715), array("guid"=>$this->migracion["guid"],"quest"=>12716),
            array("guid"=>$this->migracion["guid"],"quest"=>12717), array("guid"=>$this->migracion["guid"],"quest"=>12719),
            array("guid"=>$this->migracion["guid"],"quest"=>12720), array("guid"=>$this->migracion["guid"],"quest"=>12722),
            array("guid"=>$this->migracion["guid"],"quest"=>12723), array("guid"=>$this->migracion["guid"],"quest"=>12724),
            array("guid"=>$this->migracion["guid"],"quest"=>12725), array("guid"=>$this->migracion["guid"],"quest"=>12727),
            array("guid"=>$this->migracion["guid"],"quest"=>12733), array("guid"=>$this->migracion["guid"],"quest"=>12738),
            array("guid"=>$this->migracion["guid"],"quest"=>12747), array("guid"=>$this->migracion["guid"],"quest"=>13189),
            array("guid"=>$this->migracion["guid"],"quest"=>13188), array("guid"=>$this->migracion["guid"],"quest"=>12751),
            array("guid"=>$this->migracion["guid"],"quest"=>12754), array("guid"=>$this->migracion["guid"],"quest"=>12755),
            array("guid"=>$this->migracion["guid"],"quest"=>12756), array("guid"=>$this->migracion["guid"],"quest"=>12757),
            array("guid"=>$this->migracion["guid"],"quest"=>12778), array("guid"=>$this->migracion["guid"],"quest"=>12779),
            array("guid"=>$this->migracion["guid"],"quest"=>12800), array("guid"=>$this->migracion["guid"],"quest"=>12801),
            array("guid"=>$this->migracion["guid"],"quest"=>12842), array("guid"=>$this->migracion["guid"],"quest"=>12848),
            array("guid"=>$this->migracion["guid"],"quest"=>12849), array("guid"=>$this->migracion["guid"],"quest"=>12850),
            array("guid"=>$this->migracion["guid"],"quest"=>13165), array("guid"=>$this->migracion["guid"],"quest"=>13166)
        );
        $this->db->insert_batch("character_queststatus_rewarded",$data);
    }

    public function insertarGlifos($data = array()){
        foreach($data as $key => $value) {
            $this->insert("character_glyphs",array(
                "guid" => $this->migracion["guid"],
                "spec" =>   (int)$key,
                "glyph1" => (int)$this->switch->obtenerGlyphID($value[0][0]),
                "glyph2" => (int)$this->switch->obtenerGlyphID($value[0][1]),
                "glyph3" => (int)$this->switch->obtenerGlyphID($value[0][2]),
                "glyph4" => (int)$this->switch->obtenerGlyphID($value[1][0]),
                "glyph5" => (int)$this->switch->obtenerGlyphID($value[1][1]),
                "glyph6" => (int)$this->switch->obtenerGlyphID($value[1][2])
            ));
        }

    }

    public function insertarLogros($data = array()){
        foreach ($data as $key => $value){
            $this->insert("character_achievement",array(
                "guid " => $this->migracion["guid"],
                "achievement" => $value['I'],
                "date" => $value['D']
            ));
        }
    }

    public function insertarReputacion($data = array()){
        foreach($data as $key => $value) {
            $faction    = $this->switch->obtenerFactionID(mb_strtoupper($value['N'], 'UTF-8'), $this->migracion["locales"]);
            $flag       = 17;
			if($value['V'] < 0){
				$reputation = -1500;
			}
			else if($value['V'] == 0){
				$reputation = 1500;
			}
			else{
				$reputation =  $value['V'] < 1000 ? 1000 : $value['V'];
			}
            //$this->insert("character_reputation",array("guid" =>(int)$this->migracion["guid"],"faction"=>(int)$faction,"standing" =>(int)$reputation,"flags"=>(int)$flag));
			if($faction > 1 && $reputation > 1){
				$this->db->query("INSERT IGNORE INTO character_reputation(guid,faction,standing,flags) VALUES({$this->migracion["guid"]},{$faction},{$reputation},{$flag})");
			}
			if($faction == 1119 && $reputation > 1){
				  $this->insertarHijosDeHodir();
			}
        }
    }
	
	public function UpdateReputacion($data = array()){
        foreach($data as $key => $value) {
            $faction    = $this->switch->obtenerFactionID(mb_strtoupper($value['N'], 'UTF-8'), $this->migracion["locales"]);
            $flag       = 17;
			if($faction < 1){
				continue;
			}
			if($value['V'] < 0){
				$reputation = -1500;
			}
			else if($value['V'] == 0){
				$reputation = 1500;
			}
			else{
				$reputation =  $value['V'] < 1000 ? 1000 : $value['V'];
			}
			if($faction > 1 && $reputation > 1){
				$this->db->query("UPDATE character_reputation SET standing = {$reputation}, flags = {$flag} where guid = {$this->migracion["guid"]} and faction = {$faction}");
			}
			$this->db->query("UPDATE character_reputation set standing = 500 where guid = {$this->migracion["guid"]} and standing = 0");
        }
    }

	

	
    public function insertarHijosDeHodir(){
        $data = array(
            array("guid" => $this->migracion["guid"],"quest"=>12841), array("guid" => $this->migracion["guid"],"quest"=>12843),
            array("guid" => $this->migracion["guid"],"quest"=>12846), array("guid" => $this->migracion["guid"],"quest"=>12851),
            array("guid" => $this->migracion["guid"],"quest"=>12856), array("guid" => $this->migracion["guid"],"quest"=>12886),
            array("guid" => $this->migracion["guid"],"quest"=>12900), array("guid" => $this->migracion["guid"],"quest"=>12905),
            array("guid" => $this->migracion["guid"],"quest"=>12906), array("guid" => $this->migracion["guid"],"quest"=>12907),
            array("guid" => $this->migracion["guid"],"quest"=>12908), array("guid" => $this->migracion["guid"],"quest"=>12915),
            array("guid" => $this->migracion["guid"],"quest"=>12921), array("guid" => $this->migracion["guid"],"quest"=>12924),
            array("guid" => $this->migracion["guid"],"quest"=>12969), array("guid" => $this->migracion["guid"],"quest"=>12970),
            array("guid" => $this->migracion["guid"],"quest"=>12971), array("guid" => $this->migracion["guid"],"quest"=>12972),
            array("guid" => $this->migracion["guid"],"quest"=>12983), array("guid" => $this->migracion["guid"],"quest"=>12996),
            array("guid" => $this->migracion["guid"],"quest"=>12997), array("guid" => $this->migracion["guid"],"quest"=>13061),
            array("guid" => $this->migracion["guid"],"quest"=>13062), array("guid" => $this->migracion["guid"],"quest"=>13063),
            array("guid" => $this->migracion["guid"],"quest"=>13064),
        );
        $this->db->insert_batch("character_queststatus_rewarded",$data);
    }

    public function insertarSpellCharacters($data = array()){
        foreach($data as $key => $value) {
            $SkillName = mb_strtoupper($value['N'], 'UTF-8');
            if($this->switch->_CheckRiding($SkillName, $value['C'], $this->migracion["level"]))
                continue;
            $SkillID    = $this->switch->obtenerSkillID($SkillName, $this->migracion["locales"]);
            if($SkillID < 1)
                continue;
            $max        = $this->switch->_MaxValue($this->switch->RemoveRaceBonus($this->migracion["raza"], $SkillID, $value['M']), 450);
            $cur        = $this->switch->_MaxValue($this->switch->RemoveRaceBonus($this->migracion["raza"], $SkillID, $value['C']), 450);
            $SpellID    = $this->switch->obtenerSpellIDForSkill($SkillID, $max);

            if($this->switch->CheckExtraSpell($SkillID))
                $this->aprenderSpell($this->switch->GetExtraSpellForSkill($SkillID, $cur));
                $this->insert("character_skills",array("guid" => $this->migracion["guid"],"skill"=>(int)$SkillID ,"value"=>(int)$cur,"max"=> (int)$max));

            if($SpellID < 3)
                continue;

            $this->aprenderSpell($SpellID);
        }
    }

    public function misSpell($data = array()){
        foreach($data as $SpellID => $value) {
            if($this->switch->ValidarSpell($SpellID, $this->migracion["clase"])){
                $this->aprenderSpell($SpellID);
            }
        }
    }

    public function misMonturas($data = array()){
        foreach($data as $key => $SpellID) {
            $this->aprenderSpell($SpellID );
        }
    }

    public function loaderCharacters($guid){
        return $this->db->query("select guid, name, race,class, gender, level from characters where guid = ".$guid." limit 1;")->row();
    }



    function enviarItems($data = array(),$guid){
        $name = $this->db->query("select name from characters where guid = ".$guid." ")->row();

        $item_array = $data;
        $name = $name->name;
        $by10       = 1;
        $toSend     = "";
        $needSend   = count($item_array);
	
        for($i = 0; $i <= count($item_array); $i++) {
            $toSend .= $item_array[$i];
            $toSend .= " ";
            if($by10 == 10) {
                trim($this->client->executeCommand(new SoapParam('send items '.$name.' "'.$this->config->item("tituloMigracion").'" "'.$this->config->item("mensaje").'" '.$toSend.' ', 'command')));
                $needSend = $needSend - $by10;
                $by10    = 1;
                $toSend = "";
			} else if($needSend - $by10 == 0) {
               trim($this->client->executeCommand(new SoapParam('send items '.$name.' "'.$this->config->item("tituloMigracion").'" "'.$this->config->item("mensaje").'" '.$toSend.' ', 'command')));
                $toSend = "";
             } else $by10++;
         }

		$this->Auth->completarEstado($guid);
        return true;
    }

    function cambiarNombre($nombre){
        try{
            $this->client->executeCommand(new SoapParam("character rename ".$nombre." ", 'command'));
            $this->client->executeCommand(new SoapParam("kick ".$nombre." ", 'command'));
			$this->session->validarCambio = true;
        }catch (Exception $e){

        }
    }


    function validarJugadorOnline($guid,$bool){
         $query = $this->db->query("select name from characters where guid = ".$guid." and online = 1");
		 if($query->num_rows()>0){
			if($bool == true){
				 $query = $query->row();
				 $name = $query->name;
				 $this->client->executeCommand(new SoapParam("kick {$name}", 'command'));
				 $this->client->executeCommand(new SoapParam("tel name {$name} migracion1", 'command'));
			}
			$query = true;	 
         }
         else{
             $query = -1;
         }
        
         return $query;
    }



	public function validarIsPj($guid,$race,$class,$gender){
		 //$guid.$race.$class.$gender;//
		return $this->db->query("select guid from characters where guid = ".$guid." and race = ".$race." and class = ".$class."  and gender = ".$gender." ")->num_rows() > 0 ? true : false;
	}


	public function buscarMisPjs(){
		return $this->db->query("select guid,name from characters where account = ".$this->session->id." ")->result();
	}
}