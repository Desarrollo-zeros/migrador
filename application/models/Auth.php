<?php
/**
 * Created by PhpStorm.
 * User: zeros
 * Date: 4/02/2018
 * Time: 4:06 PM
 */

class Auth extends  CI_Model
{

    private $db = NULL;

    public function __construct()
    {
        parent::__construct();
        ini_set('max_execution_time', 150);
        $this->db = $this->load->database('auth', TRUE);
    }


    public function encryptAccount($username, $password)
    {
        if(!is_string($username)) { $username = ""; }
        if(!is_string($password)) { $password = ""; }
        $sha_pass_hash = sha1(strtoupper($username).':'.strtoupper($password));

        return strtoupper($sha_pass_hash);
    }


    public function validarLogin($usuario, $password){
        $val = false;
        $query = $this->db->query("select id,username from account where username = '".$usuario."' and sha_pass_hash = '".$password."' limit 1 ");
        if($query->num_rows() > 0){
            $query = $query->row();
            $this->session->usuario = $query->username;
            $this->session->id = $query->id;
            $this->session->isMaster = false;
            $this->session->validarCambio = false;
			$this->session->guid = false;
            $val = true;
        }
        return $val;
    }

    public function validarIsGm(){
        $query = $this->db->query("select id,gmlevel from account_access where id = ".$this->session->id." ");
        if($query->num_rows()>0){
            $query = $query->row();
            if($query->id == $this->config->item("idGmMigracion")){
                if($query->gmlevel == $this->config->item("gmlevel")){
                    $bool = true; //master migracion
                    $this->session->isMaster = true;
                }
                else{
                    $bool = -2; //no tienes rango
                }
            }
            else{
                $bool = -3; //eres gm pero no tienes permiso entrar
            }
        }
        else{
            $bool = true; //no eres gm
        }

        return $bool;
    }


    public function loaderRealmlist(){
        return $this->db->query("select id, name, port,address from realmlist where migracion = 1")->result();
    }

    public function loaderServidores(){
        return $this->db->query("select * from servidoresAceptado where estado = 1")->result();
    }

    public function validarServer($dominio){
        return $this->db->query("select * from servidoresAceptado where estado = 1 and dominio = '".$dominio."' ")->num_rows() > 0 ? true : false;
    }

    public function insert($tabla,$data = array()){
        return $this->db->insert($tabla,$data);
    }

    public function cambioItems($item){
        $query = $this->db->query("select *from itemsAceptados where id = ".$item." ");
        if($query->num_rows()>0){
            $query = $query->row();
            $item = $query->nuevoId;
        }
        return $item;
    }

    public function validarDump($dump){
        return $this->db->query("SELECT *FROM cuentasTransferidas WHERE dump = '".$dump."' ")->num_rows() > 0 ? true : false;
    }

    public function validarExisteMigracion($nombre,$dominio){
        return $this->db->query("SELECT *FROM cuentasTransferidas WHERE nombreViejo = '".$nombre."'  and  realmlistViejo = '".$dominio."' ")->num_rows() > 0 ? true : false;
    }


    public function loaderMigraciones(){
        if($this->session->isMaster){
          return  $this->db->query("select *from cuentasTransferidas where estado = 2")->result();
        }
        return $this->db->query("select *from cuentasTransferidas where account = ".$this->session->id." and estado > 1 ")->result();
    }

    public function completarMigracion($id){
       $this->db->where('guid', $id);
        return $this->db->update("cuentasTransferidas",array("estado"=>5));
    }

    public function eliminarMigracion($id){
        return $this->db->delete('cuentasTransferidas', array('guid' => $id));
    }


    public function enviarItems($guid){
        $item = $this->CargarItems($guid);
        return $item;
    }

    public function CargarItems($guid){
        $query = $this->db->query("select items from cuentasTransferidas where guid = ".$guid." ");
        if($query->num_rows() > 0){
            $query = $query->row();
        }else{
            $query = -1;
        }

        $item_array = explode(" ", trim($query->items));
        return $item_array;
    }

    public function validarEstadoMigracion($guid){
      return  $this->db->query("select *from cuentasTransferidas where guid = ".$guid."  and estado = 5")->num_rows() > 0 ? true : false;
    }

	public function validaMigracionCompleta($guid){
		return  $this->db->query("select *from cuentasTransferidas where guid = ".$guid."  and estado = 1")->num_rows() > 0 ? true : false;
	}

	public function completarEstado($guid){
		$this->db->query("update cuentasTransferidas set estado =1 where ".$guid." ");
	}


    function confirmarCantidadItems($item,$cantidad){
        $stackable = $this->db->query("SELECT stackable FROM cantidadItemsPermitida where id = {$item} limit 1 ");

        if($stackable->num_rows()>0){
            $stackable = $stackable->row("stackable");
            if($cantidad > $stackable){
                return $stackable;
            }
            else{
                return $cantidad;
            }
        }
        else{
            return $cantidad;
        }
    }

}